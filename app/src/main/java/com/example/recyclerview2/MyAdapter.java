package com.example.recyclerview2;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private ArrayList<Picture> mPictures;
    private Context mContext;

    public MyAdapter(ArrayList<Picture> mPictures, Context mContext) {
        this.mPictures = mPictures;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.my_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(mPictures.get(position).getUrlImatge())
                .fit()
                .centerCrop()
                .into(holder.imageView);

        holder.namePic.setText(mPictures.get(position).getNamePic());
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.putExtra("namePic", mPictures.get(holder.getAdapterPosition()).getNamePic());
                intent.putExtra("description", mPictures.get(holder.getAdapterPosition()).getDescription());
                intent.putExtra("urlImatge", mPictures.get(holder.getAdapterPosition()).getUrlImatge());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPictures.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView namePic;

        ConstraintLayout rowLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            namePic = itemView.findViewById(R.id.namePic);
            rowLayout = itemView.findViewById(R.id.rowLayout);
        }
    }
}
