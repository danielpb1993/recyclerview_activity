package com.example.recyclerview2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {
    private TextView textNamePic;
    private TextView textDescription;
    private ImageView imageViewDetail;

    String name, description, urlImatge;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        textNamePic = findViewById(R.id.namePicDetail);
        textDescription = findViewById(R.id.description);
        imageViewDetail = findViewById(R.id.imageViewDetail);

        getData();
        setData();
    }

    private void setData() {
        textNamePic.setText(name);
        textDescription.setText(description);

        Picasso.get().load(urlImatge)
                .fit()
                .centerCrop()
                .into(imageViewDetail);
    }
    private void getData() {
        if (getIntent().hasExtra("namePic") && getIntent().hasExtra("description")) {
            name = getIntent().getStringExtra("namePic");
            description = getIntent().getStringExtra("description");
            urlImatge = getIntent().getStringExtra("urlImatge");
        } else {
            Toast.makeText(DetailActivity.this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }
}